using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    #region Singleton

    public static MusicController Instance { get; private set; }

	#endregion

	[SerializeField]
    private AudioSource source;
    [SerializeField]
    private AudioClip music;

	private void Awake()
	{
        if (Instance != null) Destroy(this.transform.gameObject);
        else Instance = this;

        GameObject.DontDestroyOnLoad(this.transform.gameObject);
	}

	// Start is called before the first frame update
	void Start()
    {

        source.clip = music;
        source.Play();
    }
}
