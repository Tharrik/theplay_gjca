using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingController : MonoBehaviour
{
	[SerializeField]
	public string menuSceneName = "Menu";

	#region API

	public void BackButtonAction()
	{
		SceneManager.LoadScene(menuSceneName);
	}

	#endregion

}
