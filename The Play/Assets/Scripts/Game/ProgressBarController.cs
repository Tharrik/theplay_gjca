using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarController : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private int zeroLength = 0;
    private int fullLength;
    private int progressLength;

	private void Awake()
	{
        GameManager.Instance.InitializeEvent += Initialize;
        fullLength = (int)image.rectTransform.rect.width;
	}

	// Start is called before the first frame update
	void Start()
    {
        if (image == null)
            Debug.LogError(string.Concat(name, ": UI Image missing!"));
    }

    private void Initialize()
	{
        progressLength = fullLength - zeroLength * 2;
        UpdateLength(0);
    }

	#region API

	public void UpdateLength(float progress)
	{
		image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, zeroLength + progressLength * progress);
        //Debug.Log(string.Concat(name, ": progress bar: ", progress));
        //Debug.Log(string.Concat(name, ": progress bar length: ", zeroLength + progressLength * progress));
    } 

	#endregion

}
