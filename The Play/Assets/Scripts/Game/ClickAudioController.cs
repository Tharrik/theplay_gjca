using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAudioController : MonoBehaviour
{
    [SerializeField]
    private AudioSource source;
    [SerializeField]
    private AudioClip clickAudio;

	[SerializeField]
	private float pitchVariation = 0.1f;
	private float initialPitch;

	private void Awake()
	{
		MouseClickRegister.ClickEvent += PlayClickSound;
	}

	private void Start()
	{
		source.clip = clickAudio;

		initialPitch = source.pitch;
	}


	public void PlayClickSound()
	{
		source.pitch = initialPitch;
		source.pitch += Random.Range(-pitchVariation, pitchVariation);

		source.Play();
	}
}
