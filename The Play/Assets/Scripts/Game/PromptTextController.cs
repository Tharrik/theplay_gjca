using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromptTextController : MonoBehaviour
{
    [SerializeField]
    private Text text;

	private void Awake()
	{
        GameManager.Instance.InitializeEvent += Initialize;
    }

    private void Initialize()
	{
        text.text = LevelContainer.CurrentLevel.instructions;
	}

}
