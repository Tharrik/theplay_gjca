using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioSpriteController : MonoBehaviour
{
    public enum AnimationState { Empty, CharIn, CharOut, FrameIn, FrameOut, Idle, Work }
	public static AnimationState CurrentAnimationState { get; set; } = AnimationState.CharIn;

	[SerializeField]
    private Animator animator;
	[SerializeField]
	private Image image;

	private void Awake()
	{
		GameManager.Instance.SceneChangeEvent += SpriteOut;
		MouseClickRegister.ClickEvent += SpriteWork;
	}

	// Start is called before the first frame update
	void Start()
    {
		animator.runtimeAnimatorController = LevelContainer.CurrentLevel.animatorController;
		image.sprite = LevelContainer.CurrentLevel.emptySprite;
		SpriteIn();
		CurrentAnimationState = AnimationState.CharIn;
	}

    #region API

    public void SpriteOut()
	{
		// Animate sprite character out
		Debug.Log(string.Concat(name, ": animation character out"));
		animator.SetTrigger("Out");
		CurrentAnimationState = AnimationState.CharOut;
	}

	public void MessageEndOfSceneChange()
	{
		GameManager.Instance.CallInitialize();
	}

	// Called in the middle of frame in/out animations
    public void FrameChange()
	{
		if(GameManager.Instance.HasToEnd)
		{
			GameManager.Instance.LoadEnding();
		}
		else
		{
			animator.runtimeAnimatorController = LevelContainer.CurrentLevel.animatorController;
			image.overrideSprite = LevelContainer.CurrentLevel.emptySprite;
			image.sprite = LevelContainer.CurrentLevel.emptySprite;
		}
	}

	public void SpriteIn()
	{
		Debug.Log(string.Concat(name, ": animation character in"));
		image.overrideSprite = null;
		animator.SetTrigger("In");
		CurrentAnimationState = AnimationState.CharIn;
	}

	public void SpriteIdle()
	{
		Debug.Log(string.Concat(name, ": animation idling"));
		animator.SetTrigger("Idle");
		CurrentAnimationState = AnimationState.Idle;
	}

	public void SpriteWork()
	{
		if (CurrentAnimationState == AnimationState.Work) return;
		Debug.Log(string.Concat(name, ": animation working"));
		animator.SetTrigger("Work");
		CurrentAnimationState = AnimationState.Work;
	}

	public void SetAnimationState(AnimationState state)
	{
		CurrentAnimationState = state;
	}

	#endregion
}
