using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Level
{
    // Level scenario sprites
    public RuntimeAnimatorController animatorController;
    public Sprite emptySprite;

    // Level prompt
    public string instructions = "";

    // Clicks needed
    public int clicksNeeded = 100;

    // Time available
    public int timeInSeconds = 10;

}
