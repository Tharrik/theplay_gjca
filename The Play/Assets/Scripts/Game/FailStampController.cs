using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FailStampController : MonoBehaviour
{
	[SerializeField]
	private RectTransform UIParent;
    [SerializeField]
    private GameObject failStampPrefab;

    [SerializeField]
    private RectTransform stampArea;
	public int stampXMin;
	public int stampXMax;
	public int stampYMin;
	public int stampYMax;

	private void Awake()
	{
		GameManager.Instance.SceneChangeEvent += Stamp;
	}

	private void Start()
	{ 
		stampXMin = 0;
		stampXMax = (int)(stampArea.sizeDelta.x / 2);
		stampYMin = 0;
		stampYMax = (int)(stampArea.sizeDelta.y / 2);
	}

	#region API

	public void Stamp()
	{
		int x = Random.Range(stampXMin,stampXMax);
		int y = Random.Range(stampYMin,stampYMax);

		StampAt(x, y);
	}

	public void StampAt(int x, int y)
	{
		StampAt(new Vector2(x, y));
	}

    public void StampAt(Vector2 position)
	{
		GameObject newStamp = GameObject.Instantiate(failStampPrefab);

		newStamp.transform.SetParent(UIParent);
		newStamp.transform.localPosition = position;
	}

	#endregion
}
