using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    [SerializeField]
    private Text timerText;
    private float elapsedTime;

    private bool isActive;

    public enum Period { First, Second, Third };
    public Period CurrentPeriod { get; private set; }

	#region Unity Events

	private void Awake()
	{
		// Subscribe to scene initialize event
		GameManager.Instance.InitializeEvent += Initialize;
	}

	private void Start()
	{
		if (timerText == null)
			Debug.LogWarning(string.Concat(name, ": missing timer text!"));
	}

	// Update is called once per frame
	void Update()
	{
		if (!isActive) return;

		elapsedTime += Time.deltaTime;

		UpdateTimer();

		CheckPeriod();
	}

	#endregion

	#region Internal

	private void Initialize()
	{
		elapsedTime = 0f;
		isActive = true;
	}

	private void UpdateTimer()
	{
		int seconds = LevelContainer.CurrentLevel.timeInSeconds - (int)Mathf.Ceil(elapsedTime);
		int centiSeconds = (int)((1 - elapsedTime + Mathf.Floor(elapsedTime)) * 100);
		SetTimerText(seconds, centiSeconds);
	}

	private void CheckPeriod()
	{
		switch (CurrentPeriod)
		{
			case Period.First:
				if (elapsedTime > LevelContainer.CurrentLevel.timeInSeconds / (float)3)
				{
					CurrentPeriod = Period.Second;
					GameManager.Instance.CallSecondPeriod();
				}
				break;
			case Period.Second:
				if (elapsedTime > LevelContainer.CurrentLevel.timeInSeconds / (float)3 * 2)
				{
					CurrentPeriod = Period.Third;
					GameManager.Instance.CallThirdPeriod();
				}
				break;
			case Period.Third:
				if (elapsedTime >= LevelContainer.CurrentLevel.timeInSeconds)
				{
					elapsedTime = LevelContainer.CurrentLevel.timeInSeconds;
					isActive = false;
					SetTimerText(0, 0);
					GameManager.Instance.ChangeScenario();
				}
				break;
		}
	}

	private void SetTimerText(int seconds, int centiSeconds)
	{
		timerText.text = string.Format("{0:D2}:{1:D2}", seconds, centiSeconds);
	}

	#endregion
}
