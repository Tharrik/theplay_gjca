using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelContainer : MonoBehaviour
{
    #region Singleton

    private static LevelContainer instance;

    public static Level CurrentLevel { get => instance.levels  [instance.currentLevelIndex]; }

    public static void AddLevel(Level level)
	{
        instance.levels.Add(level);
	}

	public static bool NextLevel()
	{
        ++instance.currentLevelIndex;

        return instance.currentLevelIndex < instance.levels.Count;
    } 

	#endregion

	[SerializeField]
    private List<Level> levels;
    private int currentLevelIndex = 0;

	private void Awake()
	{
        // Only one instance is allowed
        if (instance != null)
        {
            Destroy(this.transform.gameObject);
            return;
        }

        instance = this;
	}

	private void OnDestroy()
	{
        // Liberate instance
        if(instance == this)
            instance = null;
	}
}
