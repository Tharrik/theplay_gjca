using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseClickRegister : MonoBehaviour
{
	#region Click control fields
	[SerializeField]
	private int maxCPS = 6;
	[SerializeField]
	private int clickIncrement = 2;
	[SerializeField]
	private float updateTime = 0.1f;

	private int clicks;
	private float clicksNeeded;
	private float timeSinceLastClick;
	private float clicsPerSecond;
	#endregion

	#region Progress bar fields
	[SerializeField]
	private ProgressBarController progressBar;
	private float progress;
	#endregion

	public static event GameManager.SceneEvent FirstClickEvent;
	public static event GameManager.SceneEvent ClickEvent;

	private void Awake()
	{
		GameManager.Instance.InitializeEvent += Initialize;
	}

	// Start is called before the first frame update
	void Start()
	{
		if (progressBar == null)
			Debug.LogError(string.Concat(this.name, ": progress bar controller missing!"));
	}

	private void Initialize()
	{
		StopAllCoroutines();

		// Progress bar values
		progress = 0f;

		// Click control values
		clicksNeeded = LevelContainer.CurrentLevel.clicksNeeded;
		clicks = 0;
		timeSinceLastClick = 0f;
	}

	// Update is called once per frame
	void Update()
	{
		if (GameManager.Instance.CurrentState == GameManager.SceneState.ScenarioChange) return;

		// Update time
		timeSinceLastClick += Time.deltaTime;

		// Register mouse left click
		if (Input.GetMouseButtonUp(0))
		{
			if (clicks == 0)
			{
				if(FirstClickEvent != null)
					FirstClickEvent();
				StartCoroutine(AddClicks());
			}

			// Count click
			++clicks;
			ClickEvent();

			if (clicks >= clicksNeeded)
				clicks = (int)(clicksNeeded - 1);

			// Calculate CPS
			clicsPerSecond = 1 / timeSinceLastClick;

			timeSinceLastClick = 0f;
		}

		// Update progress bar
		UpdateProgressBar();
	}

	private void UpdateProgressBar()
	{
        // Update vs clicks
		progress = clicks / clicksNeeded;
		// Debug.Log(string.Concat(name, ": ", clicks, "/", clicksNeeded, " clicks"));
		progressBar.UpdateLength(progress);
	}

    private IEnumerator AddClicks()
	{
        while(true)
		{
			// Penalty clicks for auto click
			float penaltyClicks = Mathf.Max(0, (int)clicsPerSecond - maxCPS);

			// Calculate added clicks
			float addedClicks = (progress * clickIncrement + penaltyClicks) * updateTime;

            clicksNeeded += addedClicks;

            yield return new WaitForSeconds(updateTime);
		}
	}

}
