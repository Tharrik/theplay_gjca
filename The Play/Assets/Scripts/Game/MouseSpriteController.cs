using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSpriteController : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

	private void Awake()
	{
		GameManager.Instance.InitializeEvent += AnimateSlow;
		GameManager.Instance.SecondPeriodEvent += AnimateMedium;
		GameManager.Instance.ThirdPeriodEvent += AnimateFast;
		GameManager.Instance.SceneChangeEvent += AnimateStop;
	}

	// Start is called before the first frame update
	void Start()
    {
        if (animator == null)
            Debug.LogWarning(string.Concat(this.name, ": Animator is missing!"));
    }

	#region API

	public void AnimateStop()
	{
		Debug.Log(string.Concat(name, ": stop animation."));
		animator.SetTrigger("Stop");
	}

	public void AnimateSlow()
	{
		Debug.Log(string.Concat(name, ": slow animation."));
        animator.SetTrigger("Slow");
	}

    public void AnimateMedium()
	{
		Debug.Log(string.Concat(name, ": medium animation."));
		animator.SetTrigger("Medium");
    }

    public void AnimateFast()
	{
		Debug.Log(string.Concat(name, ": fast animation."));
        animator.SetTrigger("Fast");
	}

	#endregion
}
