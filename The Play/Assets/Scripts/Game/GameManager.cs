using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Singleton

    private static GameManager instance;
    public static GameManager Instance
	{
        get => instance;
	}

	#endregion

	public enum SceneState { None, First, Second, Third, ScenarioChange };
	public SceneState CurrentState { get; private set; } = SceneState.ScenarioChange;
	private SceneState nextState = SceneState.ScenarioChange;

	public delegate void SceneEvent();

    public event SceneEvent InitializeEvent;
	public event SceneEvent SecondPeriodEvent;
	public event SceneEvent ThirdPeriodEvent;
	public event SceneEvent SceneChangeEvent;

	[SerializeField]
	private string endingSceneName;
	public bool HasToEnd { get; private set; }

	private void Awake()
	{
        if (instance != null)
			Destroy(transform.gameObject);
        else
            instance = this;
	}

	// Start is called before the first frame update
	void Start()
    {
		Initialize();
    }

	// Update is called once per frame
	void Update()
    {
		CheckState();
    }

	private void Initialize()
	{
		if(InitializeEvent != null)
		{
			InitializeEvent();
		}
	}

	private void CheckState()
	{
		if (CurrentState == nextState) return;

		CurrentState = nextState;

		switch(CurrentState)
		{
			case SceneState.None:
				EventNone();
				break;
			case SceneState.First:
				EventFirst();
				break;
			case SceneState.Second:
				EventSecond();
				break;
			case SceneState.Third:
				EventThird();
				break;
			case SceneState.ScenarioChange:
				EventSceneChange();
				break;
		}
	}

	// Called when the state of the scene is none.
	private void EventNone() {}

	// Called when the first third of the time starts
	private void EventFirst()
	{
		if(InitializeEvent != null)
			InitializeEvent();
	}

	// Called when the second third of the time starts
	private void EventSecond()
	{
		if(SecondPeriodEvent != null)
			SecondPeriodEvent();
	}

	// Called when the third third of the time starts
	private void EventThird()
	{
		if(ThirdPeriodEvent != null)
			ThirdPeriodEvent();
	}

	// Called when the scece has to change
	private void EventSceneChange()
	{
		if(SceneChangeEvent != null) 
			SceneChangeEvent();

		HasToEnd = !LevelContainer.NextLevel();
	}

	#region API

	public void CallInitialize()
	{
		nextState = SceneState.First;
	}

	public void CallSecondPeriod()
	{
		nextState = SceneState.Second;
	}

	public void CallThirdPeriod()
	{
		nextState = SceneState.Third;
	}

	public void ChangeScenario()
	{
		nextState = SceneState.ScenarioChange;
	}

	public void LoadEnding()
	{
		SceneManager.LoadScene(endingSceneName);
	}

	#endregion

}
