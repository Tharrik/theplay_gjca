using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
	[SerializeField]
	private Button PlayButton;
	[SerializeField]
	private Button InstructionsButton;
	[SerializeField]
	private Button ExitButton;

	[SerializeField]
	private Text InstructionsText;
	[SerializeField]
	private Button BackButton;

	[SerializeField]
	private string gameSceneName = "Game";

	public enum MenuState { Main, Instructions };
    public MenuState CurrentState { get; private set; } = MenuState.Main;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		switch (CurrentState)
		{
			case MenuState.Main:
				ShowMain();
				break;
			case MenuState.Instructions:
				ShowInstructions();
				break;
		}
	}

	private void ShowMain()
	{
		PlayButton.gameObject.SetActive(true);
		InstructionsButton.gameObject.SetActive(true);
		ExitButton.gameObject.SetActive(true);

		InstructionsText.gameObject.SetActive(false);
		BackButton.gameObject.SetActive(false);
	}

	private void ShowInstructions()
	{
		PlayButton.gameObject.SetActive(false);
		InstructionsButton.gameObject.SetActive(false);
		ExitButton.gameObject.SetActive(false);

		InstructionsText.gameObject.SetActive(true);
		BackButton.gameObject.SetActive(true);
	}

	#region API

	public void Play()
	{
		SceneManager.LoadScene(gameSceneName);
	}

	public void BackButtonAction()
	{
		CurrentState = MenuState.Main;
	}

	public void InstructionsButtonAction()
	{
		CurrentState = MenuState.Instructions;
	}

	public void Exit()
	{
		Application.Quit();
	}

	#endregion

}
