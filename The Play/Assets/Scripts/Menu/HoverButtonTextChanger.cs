using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HoverButtonTextChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler
{
	[SerializeField]
	private Text buttonText;
	[SerializeField]
	private Color hoverColor;

	private Color initialColor;

	private void Start()
	{
		initialColor = buttonText.color;
	}

	void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
	{
		//Debug.Log(string.Concat(name, ": mouse enter."));
		buttonText.color = hoverColor;
	}

	void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
	{
		//Debug.Log(string.Concat(name, ": mouse exit."));
		buttonText.color = initialColor;
	}

	void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
	{
		buttonText.color = initialColor;
	}
}
